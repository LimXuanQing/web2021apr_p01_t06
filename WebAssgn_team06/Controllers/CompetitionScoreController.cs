﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.DAL;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.Controllers
{
    public class CompetitionScoreController : Controller
    {
        private CompetitionScoreDAL competitionScoreContext = new CompetitionScoreDAL();
        private CriteriaDAL criteriaContext = new CriteriaDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CompetitionSubmissionDAL competitionSubmissionContext = new CompetitionSubmissionDAL();
        private CompetitorDAL competitorContext = new CompetitorDAL();
        

        // GET: CompetitionScoreController
        public ActionResult Index(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            //HERE
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            CompetitionScoreViewModel competitionScoreVM = new CompetitionScoreViewModel();
            competitionScoreVM.competitionList = competitionContext.GetAllCompetition();
            if (id != null)
            {
                ViewData["selectedCompetitionId"] = id.Value;
                competitionScoreVM.competitionSubmissionsList = competitionContext.GetCompetitionSubmission(id.Value);
                HttpContext.Session.SetInt32("CompetitionId", (int)id);
            }
            else
            {
                ViewData["selectedCompetitionId"] = "";
            }

            return View(competitionScoreVM);
        }

        public ActionResult Index2(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<CompetitionScore> scoreList = competitionScoreContext.GetAllCompetitionScore();
            ViewData["selectedCompetitionId"] = id;
            if (id != null)
            {
                HttpContext.Session.SetInt32("CompetitionId", (int)id);
            }

            return View(scoreList);
        }

        public ActionResult Index3(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<CompetitionScore> scoreList = competitionScoreContext.GetAllCompetitionScore();
            ViewData["selectedCompetitionId"] = id;
            if (id != null)
            {
                HttpContext.Session.SetInt32("CompetitionId", (int)id);
            }

            return View(scoreList);
        }

        public ActionResult SubmissionScore(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            CompetitionScoreViewModel competitionScoreVM = new CompetitionScoreViewModel();
            if (id != null)
            {
                ViewData["selectedCompetitorId"] = id.Value;
                competitionScoreVM.competitionScoreList = competitionSubmissionContext.GetCompetitionSubmissionScore(id.Value);
            }
            else
            {
                ViewData["selectedCompetitorId"] = "";
            }

            return View(competitionScoreVM);
        }

        // GET: CompetitionScoreController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CompetitionScoreController/Create
        public ActionResult Create(int? id, int? compId)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }

            ViewData["CompetitorCriteriaList"] = GetCompetitorCriteria3(compId);
            ViewData["CompetitionId"] = compId;
            ViewData["CompetitorId"] = id;
            return View();
        }
        

        // POST: CompetitionScoreController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompetitionScore cs)
        {
            ViewData["CompetitorCriteriaList"] = GetCompetitorCriteria3(HttpContext.Session.GetInt32("CompetitionId"));
            ViewData["CompetitionId"] = HttpContext.Session.GetInt32("CompetitionId");
            if (ModelState.IsValid)
            {
                //Add competition score to database
                cs.CompetitorId = competitionScoreContext.Add(cs);
                //Redirect user to CompetitionScore/Index2 view
                return RedirectToAction("Index2");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(cs);
            }
        }


        // GET: CompetitionScoreController/Edit/5
        public ActionResult Edit(int? id, int? compId)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            CompetitionScore competitionScore = competitionScoreContext.GetDetails(id.Value,compId.Value);
            if (competitionScore == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }

            ViewData["CompetitorCriteriaList"] = GetCompetitorCriteria3(HttpContext.Session.GetInt32("CompetitionId"));
            //ViewData["CompetitionId"] = HttpContext.Session.GetInt32("CompetitionId");
            return View(competitionScore);
        }

        //Get all coriteria for competition
        private List<Criteria> GetCompetitorCriteria(int? id)
        {
            List<CompetitionScore> competitionScoreList = competitionScoreContext.GetAllCompetitionScore();
            List<Criteria> criteriaList = criteriaContext.GetAllCriteria();
            List<Criteria> competitorCriteriaList = new List<Criteria>();
            for (int i = 0; i < competitionScoreList.Count; i++)
            {
                if (competitionScoreList[i].CompetitorId == id)
                {
                    for (int j = 0; j < criteriaList.Count; j++)
                    {
                        if (criteriaList[j].CriteriaId == competitionScoreList[i].CriteriaId)
                        {
                            competitorCriteriaList.Add(criteriaList[j]);
                        }
                    }
                }
            }
            return competitorCriteriaList;
        }

        private List<Criteria> GetCompetitorCriteria2(int? id)
        {
            List<CompetitionScore> competitionScoreList = competitionScoreContext.GetAllCompetitionScore();
            List<Criteria> criteriaList = criteriaContext.GetAllCriteria();
            List<Criteria> competitorCriteriaList = new List<Criteria>();
            List<CompetitionSubmission> competitionSubmissionList = competitionSubmissionContext.GetAllCompetitionSubmission();
            for (int i = 0; i < competitionSubmissionList.Count; i++)
            {
                if (competitionSubmissionList[i].CompetitionId == id)
                {
                    for (int j = 0; j < criteriaList.Count; j++)
                    {
                        competitorCriteriaList.Add(criteriaList[j]);
                    }
                }
            }
            return competitorCriteriaList;
        }

        private List<Criteria> GetCompetitorCriteria3(int? id)
        {
            List<CompetitionScore> competitionScoreList = competitionScoreContext.GetAllCompetitionScore();
            List<Criteria> criteriaList = criteriaContext.GetAllCriteria();
            List<Criteria> competitorCriteriaList = new List<Criteria>();
            for (int i = 0; i < criteriaList.Count; i++)
            {
                if (criteriaList[i].CompetitionId == id)
                {
                    competitorCriteriaList.Add(criteriaList[i]);
                }
            }
            return competitorCriteriaList;
        }

        // POST: CompetitionScoreController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompetitionScore competitionScore)
        {
            ViewData["CompetitorCriteriaList"] = GetCompetitorCriteria3(competitionScore.CompetitionId);
            //ViewData["CompetitionId"] = HttpContext.Session.GetInt32("CompetitionId");
            if (ModelState.IsValid)
            {
                //Update staff record to database
                competitionScoreContext.Update(competitionScore);
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message
                return View(competitionScore);
            }
        }

        // GET: CompetitionScoreController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CompetitionScoreController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        public ActionResult IndexCompetitor2(int? id, int? cid)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Competitor" role
            //HERE
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<CompetitionScore> scoreList = competitionScoreContext.GetAllCompetitionScore2();
            ViewData["selectedCompetitionId"] = id;
            
            if (id != null)
            {
                HttpContext.Session.SetInt32("CompetitionId", (int)id);
            }

            return View(scoreList);
        }
        public ActionResult IndexCompetitor(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Competitor" role
            //HERE
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            CompetitionScoreViewModel competitionScoreVM = new CompetitionScoreViewModel();
            competitionScoreVM.competitionList = competitionContext.GetAllCompetition();
            if (id != null)
            {
                ViewData["selectedCompetitionId"] = id.Value;
                competitionScoreVM.competitionSubmissionsList = competitionContext.GetCompetitionSubmission(id.Value);
                HttpContext.Session.SetInt32("CompetitionId", (int)id);
                ViewData["CompetitorId"] = HttpContext.Session.GetInt32("CompetitorId");
            }
            else
            {
                ViewData["selectedCompetitionId"] = "";
                ViewData["selectedCompetitorId"] = "";
            }

            return View(competitionScoreVM);
        }
    }
}
