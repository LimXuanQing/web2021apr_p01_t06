﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.DAL;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.Controllers
{   
    public class CompetitionCriteriaController : Controller
    {
        private CriteriaDAL criteriaContext = new CriteriaDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();

        public ActionResult Index(int? id)
        {

            // Stop accessing the action if not logged in
            // or account not in the "Competitor" role
            //Used for Criteria View Model
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            CriteriaViewModel criteriaVM = new CriteriaViewModel();
            criteriaVM.competitionList = competitionContext.GetAllCompetition();
            // Check if CompetitionID (id) presents in the query string
            if (id != null)
            {
                ViewData["selectedCompetitionId"] = id.Value;
                // Get list of staff working in the branch
                criteriaVM.criteriaList = competitionContext.GetCompetitionCriteria(id.Value);
                int weightage = GetTotalWeightage(criteriaVM.criteriaList);
                ViewData["totalWeightage"] = weightage;
            }
            else
            {
                ViewData["selectedCompetitionId"] = "";
            }
            return View(criteriaVM);
        }

        public ActionResult Competition()
        {
            return View();
        }

        // GET: CriteriaController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CriteriaController/Create
        public ActionResult Create(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Competitor" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            ViewData["selectedCompetitionId"] = id.Value;
            ViewData["CompetitionList"] = GetCompetition();
            //ViewData["CompetitionCriteriaList"] = GetCompetitionCriteria(id);
            return View();
        }

        private List<Competition> GetCompetition()
        {
            // Get a list of competitiont from database
            List<Competition> competitionList = competitionContext.GetAllCompetition();
            // Adding a select prompt at the first row of the competition list
            competitionList.Insert(0, new Competition
            {
                CompetitionId = 0,
                CompetitionName = "--Select--"
            });
            return competitionList;
        }

        private List<Criteria> GetCompetitionCriteria(int? id)
        {
            List<Criteria> criteriaList = criteriaContext.GetAllCriteria();
            List<Criteria> competitionCriteriaList = new List<Criteria>();
            for (int i = 0; i < criteriaList.Count; i++)
            {
                if (criteriaList[i].CompetitionId == id)
                {
                    competitionCriteriaList.Add(criteriaList[i]);
                }
            }
            return competitionCriteriaList;
        }

        private int GetTotalWeightage(List<Criteria> cList)
        {
            List<int> weightage = new List<int>();
            for (int i = 0; i < cList.Count; i++)
            {
                //int weightage = 0;
                weightage.Add(cList[i].Weightage);
                int result = weightage.Sum();
                return result;
            }
            return 0;
        }

        // POST: CriteriaController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Criteria criteria)
        {
            // Get competition list for drop - down list
            //in case of the need to return to Create.cshtml view
            ViewData["CompetitionList"] = GetCompetition();

            if (ModelState.IsValid)
            {
                //Add criteria record to database
                criteria.CriteriaId = criteriaContext.Add(criteria);
                //Redirect user to Staff/Index view
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(criteria);
            }
        }

        // GET: CriteriaController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CriteriaController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // POST: CriteriaController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Criteria criteria)
        {
            // Delete the criteria record from database
            criteriaContext.Delete(criteria.CriteriaId);
            return RedirectToAction("Index");
        }
    }
}

