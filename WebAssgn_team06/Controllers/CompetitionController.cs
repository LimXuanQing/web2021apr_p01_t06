﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.DAL;
using WebAssgn_team06.Models;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace WebAssgn_team06.Controllers
{
    public class CompetitionController : Controller
    {
        private CompetitionDAL cContext = new CompetitionDAL();
        private AreaInterestDAL areaInterestContext = new AreaInterestDAL();

        private CriteriaDAL criteriaContext = new CriteriaDAL();
        // GET: CompetitionController
        public ActionResult Index()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<Competition> cList = cContext.GetAllCompetition();
            return View(cList);
        }

        // GET: CompetitionController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CompetitionController/Create
        public ActionResult Create()
        {
            // Stop accessing the action if not logged in
            // or account not in the "admin" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            ViewData["AreaInterestList"] = GetAreaInterest();
            return View();
        }
        private List<AreaInterest> GetAreaInterest()
        {
            // Get a list of area interest from database
            List<AreaInterest> areaInterestList = areaInterestContext.GetAllAreaInterest();

            return areaInterestList;
        }



        // POST: CompetitionController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Competition c)
        {
            //Get country list for drop-down list
            //in case of the need to return to Create.cshtml view
            ViewData["AreaInterestList"] = GetAreaInterest();
            if (ModelState.IsValid)
            {
                //Add staff record to database
                c.CompetitionId = cContext.Add(c);
                //Redirect user to Competition/Index view
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(c);
            }
        }

        // GET: CompetitionController/CreateCriteria
        public ActionResult CreateCriteria()
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role

            //here edit!!
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            //To access competition list
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            ViewData["CompetitionList"] = cContext.GetAllCompetition();

            return View();

        }

        //Xuan Qing
        //// POST: CompetitionController/CreateCriteria
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult CreateCriteria(CriteriaViewModel cvm)
        //{
        //    // Get competition list for drop - down list
        //    //in case of the need to return to Create.cshtml view
        //    ViewData["CompetitionList"] = cContext.GetAllCompetition();

        //    if (ModelState.IsValid)
        //    {
        //        //save your Model1 data
        //        foreach (Criteria criteria in cvm.Criterias)
        //        {
        //            cvm.Criterias.Add(criteria);
        //            //Save your SubItems here
        //            //Add criteria record to database
        //            criteria.CriteriaId = criteriaContext.Add(criteria);
        //        }

        //        //Redirect user to Staff/Index view
        //        return RedirectToAction("Index");
        //    }
        //    else
        //    {
        //        //Input validation fails, return to the Create view
        //        //to display error message
        //        return View();
        //    }
        //}

        // GET: CompetitionController/Edit/5
        public ActionResult Edit(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "admin" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            ViewData["AreaInterestList"] = GetAreaInterest();
            Competition c = cContext.GetDetails(id.Value);
            if (c == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(c);
        }

        // POST: CompetitionController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Competition c)
        {
            ViewData["AreaInterestList"] = GetAreaInterest();
            if (cContext.HasCompetitor(c.CompetitionId) == false)
            {
                if (ModelState.IsValid)
                {
                    //Update competition record to database
                    cContext.Update(c);
                    return RedirectToAction("Index");
                }
                else
                {
                    //Input validation fails, return to the view
                    //to display error message
                    return View(c);
                }
            }
            else
            {
                //validation message
                String errorMsg = "Competition has competitors joined. Unable to edit competition.";
                ViewBag.validation = errorMsg;
                return View();
            }

        }

        // GET: CompetitionController/Delete/5
        public ActionResult Delete(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "admin" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            Competition c = cContext.GetDetails(id.Value);
            if (c == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(c);
        }

        // POST: CompetitionController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Competition c)
        {
            if (cContext.HasCompetitor(c.CompetitionId) == false)
            {
                // Delete the competition record from database
                cContext.Delete(c.CompetitionId);
                return RedirectToAction("Index");
            }
            else
            {
                //validation message
                String errorMsg = "Competition has competitors joined. Unable to delete competition.";
                ViewBag.validation = errorMsg;
                return View();
            }
        }
    }
}