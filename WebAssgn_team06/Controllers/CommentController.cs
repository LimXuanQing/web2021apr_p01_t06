﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.DAL;
using WebAssgn_team06.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;

namespace WebAssgn_team06.Controllers
{
    public class CommentController : Controller
    {
        private CommentDAL ctContext = new CommentDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();

        public ActionResult Index(int? id)
        {
            List<Comment> ctList = ctContext.GetAllComment();
            CommentViewModel commentVM = new CommentViewModel();
            commentVM.competitionList = competitionContext.GetAllCompetition();
            if (id != null)
            {
                ViewData["selectedCompetitionId"] = id.Value;
                commentVM.commentList = competitionContext.GetCompetitionComment(id.Value);
            }
            else
            {
                ViewData["selectedCompetitionId"] = "";
            }
            return View(commentVM);
        }

        // GET: CommentController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CommentController/Create
        public ActionResult Create(int? id)
        {
            ViewData["selectedCompetitionId"] = id.Value;

            return View();
        }

        private List<Competition> GetCompetition()
        {
            // Get a list of competition from database
            List<Competition> competitionList = competitionContext.GetAllCompetition();
            // Adding a select prompt at the first row of the competition list
            competitionList.Insert(0, new Competition
            {
                CompetitionId = 0,
                CompetitionName = "--Select--"
            });
            return competitionList;
        }

        // POST: CommentController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Comment ct)
        {
            //Get country list for drop-down list
            //in case of the need to return to Create.cshtml view
            if (ModelState.IsValid)
            {
                //Add comment record to database
                ct.CommentId = ctContext.Add(ct);
                //Redirect user to Public/Index view
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(ct);
            }
        }


        // GET: CommentController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CommentController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CommentController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CommentController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
