﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.DAL;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.Controllers
{
    public class JudgeController : Controller
    {
        private JudgeDAL judgeContext = new JudgeDAL();
        private AreaInterestDAL areaInterestContext = new AreaInterestDAL();

        // GET: JudgeController
        public ActionResult Index()
        {
         
            List<Judge> judgeList = judgeContext.GetAllJudge();
            
            return View(judgeList);
        }

        // GET: JudgeController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

       

        // GET: JudgeController/Create
        public ActionResult Create()
        {
            
            ViewData["SalutationList"] = GetSalutation();
            ViewData["AreaInterestList"] = GetAreaInterest();
            return View();
        }

        private List<AreaInterest> GetAreaInterest()
        {
            // Get a list of area interest from database
            List<AreaInterest> areaInterestList = areaInterestContext.GetAllAreaInterest();
            // Adding a select prompt at the first row of the area interest list
            areaInterestList.Insert(0, new AreaInterest
            {
                AreaInterestId = 0,
                Name = "--Select--"
            });
            return areaInterestList;
        }

        private List<SelectListItem> GetSalutation()
        {
            List<SelectListItem> salutation = new List<SelectListItem>();
            salutation.Add(new SelectListItem
            {
                Value = "Dr",
                Text = "Dr"
            });
            salutation.Add(new SelectListItem
            {
                Value = "Mr",
                Text = "Mr"
            });
            salutation.Add(new SelectListItem
            {
                Value = "Ms",
                Text = "Ms"
            });
            salutation.Add(new SelectListItem
            {
                Value = "Mrs",
                Text = "Mrs"
            });
            salutation.Add(new SelectListItem
            {
                Value = "Mdm",
                Text = "Mdm"
            });
            return salutation;
        }

        // POST: JudgeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Judge judge)
        {
            //Get salutation list for drop-down list
            //in case of the need to return to Create.cshtml view
            ViewData["SalutationList"] = GetSalutation();
            ViewData["AreaInterestList"] = GetAreaInterest();
            if (ModelState.IsValid)
            {
                //Add judge profile to database
                judge.JudgeId = judgeContext.Add(judge);
                //Redirect user to Judge/Index view
                return RedirectToAction("Index","Home");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(judge);
            }
        }

        // GET: JudgeController/Edit/5
        public ActionResult Edit(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            ViewData["AreaInterestList"] = GetAreaInterest();
            ViewData["SalutationList"] = GetSalutation();
            Judge judge = judgeContext.GetDetails(id.Value);
            if (judge == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(judge);
        }

        // POST: JudgeController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Judge judge)
        {
            //Get drop-down list
            //in case of the need to return to Edit.cshtml view
            ViewData["SalutationList"] = GetSalutation();
            ViewData["AreaInterestList"] = GetAreaInterest();
            if (ModelState.IsValid)
            {
                //Update judge record to database
                judgeContext.Update(judge);
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message
                return View(judge);
            }
        }

        // GET: JudgeController/Delete/5
        public ActionResult Delete(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            Judge judge = judgeContext.GetDetails(id.Value);
            if (judge == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(judge);
        }

        // POST: JudgeController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Judge judge)
        {
            // Delete the judge record from database
            judgeContext.Delete(judge.JudgeId);
            return RedirectToAction("Index","Home");
        }
    }
}
