﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.DAL;
using WebAssgn_team06.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;

namespace WebAssgn_team06.Controllers
{
    public class CompetitionJudgeController : Controller
    {
        private CompetitionJudgeDAL cjContext = new CompetitionJudgeDAL();
        private CompetitionDAL cContext = new CompetitionDAL();
        private JudgeDAL jContext = new JudgeDAL();
        // GET: competitionjudgeController
        public ActionResult Index()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<CompetitionJudge> cjList = cjContext.GetAllCompetitionJudge();
            return View(cjList);
        }
        public ActionResult Create()
        {
            // Stop accessing the action if not logged in
            // or account not in the "Admin" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            ViewData["CompetitionList"] = GetCompetition();
            ViewData["JudgeList"] = GetJudge();

            return View();
        }

        private List<Competition> GetCompetition()
        {
            // Get a list of competition from database
            List<Competition> competitionList = cContext.GetAllCompetition();
            // Adding a select prompt at the first row of the competition list
            competitionList.Insert(0, new Competition
            {
                CompetitionId = 0,
                CompetitionName = "--Select--"
            });
            return competitionList;
        }
        private List<Judge> GetJudge()
        {
            // Get a list of judge from database
            List<Judge> judgeList = jContext.GetAllJudge();
            // Adding a select prompt at the first row of the judge list
            judgeList.Insert(0, new Judge
            {
                JudgeId = 0,
                JudgeName = "--Select--"
            });
            return judgeList;
        }

        // POST: Staff/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompetitionJudge cj)
        {
            //Get country list for drop-down list
            //in case of the need to return to Create.cshtml view
            ViewData["CompetitionList"] = GetCompetition();
            ViewData["JudgeList"] = GetJudge();

            if (ModelState.IsValid)
            {
                if (cjContext.judgeAssigned(cj.JudgeId) == false)
                {
                    if (cjContext.beforeEndDate(cj.CompetitionId) == true)
                    {

                        //Add competition judge record to database
                        cj.CompetitionId = cjContext.Add(cj);
                        cj.JudgeId = cjContext.Add(cj);
                        //Redirect user to Index view
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //validation message
                        String errorMsg = "The End Date of competition has passed. Unable to add judge.";
                        ViewBag.validation = errorMsg;
                        return View();
                    }
                }
                else
                {
                    //validation message
                    String errorMsg = "Judge already assigned. Unable to add judge.";
                    ViewBag.validation = errorMsg;
                    return View();
                }
            }



            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View();
            }
        }

        public ActionResult Delete(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "admin" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            CompetitionJudge cj = cjContext.GetDetails(id.Value);
            if (cj == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(cj);


        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(CompetitionJudge cj)
        {

            if (ModelState.IsValid)
            {
                if (cjContext.beforeEndDate(cj.CompetitionId) == true)
                {
                    // Delete the competition judge record from database
                    cjContext.Delete(cj.JudgeId);
                    return RedirectToAction("Index");
                }
                else
                {
                    String errorMsg = "The End Date of competition has passed. Unable to delete judge.";
                    ViewBag.validation = errorMsg;
                    return View();
                }
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View();
            }
        }


    }
}