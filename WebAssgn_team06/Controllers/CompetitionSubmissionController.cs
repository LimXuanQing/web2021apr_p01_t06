﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.DAL;
using WebAssgn_team06.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;
using System.IO;

namespace WebAssgn_team06.Controllers
{
    public class CompetitionSubmissionController : Controller
    {
        private CompetitionSubmissionDAL csContext = new CompetitionSubmissionDAL();
        private CompetitionDAL competitionContext = new CompetitionDAL();
        private CompetitorDAL competitorContext = new CompetitorDAL();
        private CriteriaDAL criteriaContext = new CriteriaDAL();
        private CompetitionScoreDAL competitionScoreContext = new CompetitionScoreDAL();

        // GET: CompetitionSubmissionController

        public ActionResult Index()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<CompetitionSubmission> csList = csContext.GetAllCompetitionSubmission();
            return View(csList);
        }

        public ActionResult IndexJudge()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<CompetitionSubmission> csList = csContext.GetAllCompetitionSubmission();
            return View(csList);
        }

        public ActionResult IndexPublic(int? id)
        {
            List<CompetitionSubmission> competitionSubmissionList = csContext.GetAllCompetitionSubmission();
            CompetitionResultViewModel competitionResultVM = new CompetitionResultViewModel();
            competitionResultVM.competitionList = competitionContext.GetAllCompetition();
            if (id != null)
            {
                ViewData["selectedCompetitionId"] = id.Value;
                competitionResultVM.competitionSubmissionList = competitionContext.GetCompetitionSubmission(id.Value);
                HttpContext.Session.SetInt32("CompetitionId", (int)id);
            }
            else
            {
                ViewData["selectedCompetitionId"] = "";
            }
            return View(competitionResultVM);
        }

        public ActionResult SubmissionCompetitor(int? id)
        {
            CompetitionResultViewModel competitionResultVM = new CompetitionResultViewModel();
            if (id != null)
            {
                ViewData["selectedCompetitorId"] = id.Value;
                competitionResultVM.competitorList = csContext.GetSubmissionCompetitor(id.Value);
            }
            else
            {
                ViewData["selectedCompetitorId"] = "";
            }

            return View(competitionResultVM);
        }

        public ActionResult IndexRankingPublic(int? id)
        {
            List<CompetitionSubmission> competitionSubmissionList = csContext.GetAllCompetitionSubmission();
            RankingViewModel rankingVM = new RankingViewModel();
            rankingVM.competitionList = competitionContext.GetAllCompetition();
            if (id != null)
            {
                ViewData["selectedCompetitionId"] = id.Value;
                rankingVM.competitionSubmissionList = competitionContext.GetCompetitionSubmission(id.Value);
            }
            else
            {
                ViewData["selectedCompetitionId"] = "";
            }

            return View(rankingVM);
        }

        private List<Competition> GetAllCompetition()
        {
            // Get a list of competition from database
            List<Competition> competitionList = competitionContext.GetAllCompetition();
            // Adding a select prompt at the first row of the competition list
            competitionList.Insert(0, new Competition
            {
                CompetitionId = 0,
                CompetitionName = "--Select--"
            });
            return competitionList;
        }

        // GET: CompetitionSubmissionController/Vote/5
        public ActionResult Vote(int? id)
        {
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("IndexPublic");
            }
            ViewData["CompetitionList"] = GetAllCompetition();
            CompetitionSubmission competitionSubmission = csContext.GetDetails3(id.Value, HttpContext.Session.GetInt32("CompetitionId"));
            if (competitionSubmission == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("IndexPublic");
            }

            ViewData["CompetitionId"] = HttpContext.Session.GetInt32("CompetitionId");

            List<CompetitionSubmission> competitionSubmissionList = csContext.GetAllCompetitionSubmission();
            List<Competition> competitionList = competitionContext.GetAllCompetition();
            int vote;
            int voteCount;
            foreach(var item in competitionList)
            {
                foreach(var item2 in competitionSubmissionList)
                {
                    if(item2.CompetitorId == id && item.CompetitionId == item2.CompetitionId && item2.CompetitionId == HttpContext.Session.GetInt32("CompetitionId"))
                    {
                        voteCount = item2.VoteCount + 1;
                        ViewData["VoteCount"] = voteCount;
                        HttpContext.Session.SetInt32("VoteCount", (int)voteCount);
                        vote = item2.VoteCount;
                        ViewData["Vote"] = vote;
                        HttpContext.Session.SetInt32("Vote", (int)vote);
                    }
                }
            }
            return View(competitionSubmission);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Vote(CompetitionSubmission competitionSubmission)
        {
            ViewData["Vote"] = HttpContext.Session.GetInt32("Vote");
            ViewData["VoteCount"] = HttpContext.Session.GetInt32("VoteCount");
            ViewData["CompetitionId"] = HttpContext.Session.GetInt32("CompetitionId");
            if (ModelState.IsValid)
            {
                //Update competition submssion to database
                csContext.Vote(competitionSubmission);
                //return RedirectToAction("IndexPublic");
                // Redirect to Home/Index Page
                return RedirectToAction("Index","Home");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message
                return View(competitionSubmission);
            }
        }

        // GET: CompetitionSubmissionController/Details/5
        public ActionResult Details(int id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Competitor" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            CompetitionSubmission cs = csContext.GetDetails(id);
            CompetitionSubmissionViewModel csVM = MapToCSVM(cs);
            return View(csVM);
        }

        // GET: CompetitionSubmissionController/Create
        public ActionResult Create()
        {
            // Stop accessing the action if not logged in
            // or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }


        // GET: CommentController/Create2
        public ActionResult Create2(int? id)
        {
            ViewData["selectedCompetitionId"] = id.Value;

            return View();
        }

        public ActionResult IndexRanking(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Staff" role
            //HERE
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            RankingViewModel rankingVM = new RankingViewModel();
            rankingVM.competitionList = competitionContext.GetAllCompetition();
            if (id != null)
            {
                ViewData["selectedCompetitionId"] = id.Value;
                rankingVM.competitionSubmissionList = competitionContext.GetCompetitionSubmission(id.Value);
                HttpContext.Session.SetInt32("CompetitionId", (int)id);
            }
            else
            {
                ViewData["selectedCompetitionId"] = "";
            }

            return View(rankingVM);
        }

        public ActionResult Ranking(int? id, int? compId)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("IndexRanking");
            }
            CompetitionSubmission competitionSubmission = csContext.GetDetails2(id.Value, compId.Value);
            if (competitionSubmission == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("IndexRanking");
            }

            ViewData["CompetitionId"] = compId;
            
            List<Criteria> criteriaList = criteriaContext.GetAllCriteria();
            List<CompetitionScore> competitionScoreList = competitionScoreContext.GetAllCompetitionScore();
            int score = 0;
            int weightage = 0;
            double total = 0;
            for (int i = 0; i < competitionScoreList.Count; i++)
            {
                if(competitionScoreList[i].CompetitorId == id && competitionScoreList[i].CompetitionId == HttpContext.Session.GetInt32("CompetitionId"))
                {
                    for(int j = 0; j < criteriaList.Count; j++)
                    {
                        if(competitionScoreList[i].CriteriaId == criteriaList[j].CriteriaId && competitionScoreList[i].CompetitionId == criteriaList[j].CompetitionId)
                        {
                            score = competitionScoreList[i].Score;
                            weightage = criteriaList[j].Weightage;
                            total += weightage * score / 10;
                            
                        }
                    }
                }
            }
            ViewData["TotalMark"] = total;
            HttpContext.Session.SetInt32("TotalMark", (int)total);
            return View(competitionSubmission);
        }

        // POST: CompetitionSubmissioController/Ranking
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Ranking(CompetitionSubmission competitionSubmission)
        {
            ViewData["TotalMark"] = HttpContext.Session.GetInt32("TotalMark");
            ViewData["CompetitionId"] = HttpContext.Session.GetInt32("CompetitionId");
            if (ModelState.IsValid)
            {
                //Update staff record to database
                csContext.Update(competitionSubmission);
                return RedirectToAction("IndexRanking");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message
                return View(competitionSubmission);
            }
        }

        // POST: CompetitionSubmissionController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompetitionSubmission cs)
        {
            //Get country list for drop-down list
            //in case of the need to return to Create.cshtml view
            //ViewData["CountryList"] = GetCountries();
            if (ModelState.IsValid)
            {
                //Add staff record to database
                cs.CompetitionId = csContext.Add(cs);
                //Redirect user to Staff/Index view
                //SHOULD NOT GO TO COMPETITION SUBMISSION INDEX
                return RedirectToAction("Index");
                //return RedirectToAction("Success");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(cs);
            }
        }

        
        // POST: CommentController/Create2
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create2(CompetitionSubmission cs)
        {
            //Get country list for drop-down list
            //in case of the need to return to Create.cshtml view
            //ViewData["CommentList"] = GetComment();
            //ViewData["CompetitionList"] = GetCompetition();
            if (ModelState.IsValid)
            {
                //Add staff record to database
                cs.CompetitionId = csContext.Add(cs);
                //Redirect user to Staff/Index view
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(cs);
            }
        }
        


        // GET: CompetitionSubmissionController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CompetitionSubmissioController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AreaInterestController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CompetitionSubmissioController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult UploadFile(int id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Competitor" role
            //CompetitionSubmissionViewModel
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            CompetitionSubmission cs = csContext.GetDetails(id);
            CompetitionSubmissionViewModel csVM = MapToCSVM(cs);
            return View(csVM);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadFile(CompetitionSubmissionViewModel csVM)
        {
            if (csVM.fileToUpload != null &&
            csVM.fileToUpload.Length > 0)
            {
                try
                {
                    // Find the filename extension of the file to be uploaded.
                    string fileExt = Path.GetExtension(
                     csVM.fileToUpload.FileName);
                    // Rename the uploaded file with the staff’s name.
                    string uploadedFile = csVM.CompetitorId + fileExt;
                    // Get the complete path to the images folder in server
                    string savePath = Path.Combine(
                     Directory.GetCurrentDirectory(),
                     "wwwroot\\images", uploadedFile);
                    // Upload the file to server
                    using (var fileSteam = new FileStream(
                     savePath, FileMode.Create))
                    {
                        await csVM.fileToUpload.CopyToAsync(fileSteam);
                    }
                    csVM.FileSubmitted = uploadedFile;
                    ViewData["Message"] = "File uploaded successfully.";
                }
                catch (IOException)
                {
                    //File IO error, could be due to access rights denied
                    ViewData["Message"] = "File uploading fail!";
                }
                catch (Exception ex) //Other type of error
                {
                    ViewData["Message"] = ex.Message;
                }
            }
            return View(csVM);
        }
        public CompetitionSubmissionViewModel MapToCSVM(CompetitionSubmission cs)
        {
            CompetitionSubmissionViewModel csVM = new CompetitionSubmissionViewModel
            {
                CompetitionId = cs.CompetitionId,
                CompetitorId = cs.CompetitorId,
                DateTimeFileUpload = cs.DateTimeFileUpload,
                Appeal = cs.Appeal,
                VoteCount = cs.VoteCount,
                Ranking = cs.Ranking,
                FileSubmitted = cs.CompetitorId + ".jpg"
            };
            return csVM;
        }
    }
}
