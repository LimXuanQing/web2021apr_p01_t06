﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.DAL;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.Controllers
{
    //Competitor controller is one of the controller for package 2. Where by the the calculation for the competitor
    //being able to create competitor profile, set own email account and password for login
    // and many more.

    public class CompetitorController : Controller
    {
        private CompetitorDAL competitorContext = new CompetitorDAL();
        //private AreaInterestDAL areaInterestContext = new AreaInterestDAL();

        // GET: CompetitorController
        public ActionResult Index()
        {
            // Stop accessing the action if not logged in
            // or account not in the "Competitor" role
            ///edit to Competitor later!!!!


            List<Competitor> competitorList = competitorContext.GetAllCompetitor();

            return View(competitorList);
        }

        // GET: CompetitorController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }



        // GET: CompetitorController/Create
        public ActionResult Create()
        {
            // Stop accessing the action if not logged in
            // or account not in the "Competitor" role
            /// Change to competitor!!!

            ViewData["SalutationList"] = GetSalutation();
            return View();
        }

        //salutation

        private List<SelectListItem> GetSalutation()
        {
            List<SelectListItem> salutation = new List<SelectListItem>();
            salutation.Add(new SelectListItem
            {
                Value = "Dr",
                Text = "Dr"
            });
            salutation.Add(new SelectListItem
            {
                Value = "Mr",
                Text = "Mr"
            });
            salutation.Add(new SelectListItem
            {
                Value = "Ms",
                Text = "Ms"
            });
            salutation.Add(new SelectListItem
            {
                Value = "Mrs",
                Text = "Mrs"
            });
            salutation.Add(new SelectListItem
            {
                Value = "Mdm",
                Text = "Mdm"
            });
            return salutation;
        }

        // POST: CompetitorController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Competitor competitor)
        {
            //Get salutation list for drop-down list
            //in case of the need to return to Create.cshtml view
            ViewData["SalutationList"] = GetSalutation();
            if (ModelState.IsValid)
            {
                //Add competitor profile to database
                competitor.CompetitorID = competitorContext.Add(competitor);
                //Redirect user to Competitor/Index view
                return RedirectToAction("Index", "Home");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(competitor);
            }
        }

        // GET: CompetitorController/Edit/5
        public ActionResult Edit(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            ViewData["SalutationList"] = GetSalutation();
            Competitor competitor = competitorContext.GetDetails(id.Value);
            if (competitor == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(competitor);
        }

        // POST: CompetitorController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Competitor competitor)
        {
            //Get drop-down list
            //in case of the need to return to Edit.cshtml view
            ViewData["SalutationList"] = GetSalutation();
            if (ModelState.IsValid)
            {
                //Update judge record to database
                competitorContext.Update(competitor);
                return RedirectToAction("Index");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message
                return View(competitor);
            }
        }

        // GET: CompetitorController/Delete/5
        public ActionResult Delete(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            Competitor competitor = competitorContext.GetDetails(id.Value);
            if (competitor == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(competitor);
        }

        // POST: CompetitorController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Competitor competitor)
        {
            // Delete the judge record from database
            competitorContext.Delete(competitor.CompetitorID);
            return RedirectToAction("Index", "Home");
        }
    }
}
