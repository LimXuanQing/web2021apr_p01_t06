﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.Models;
using WebAssgn_team06.DAL;
using Microsoft.AspNetCore.Authentication.Cookies;
using Google.Apis.Auth.OAuth2;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Google.Apis.Auth;
using static Google.Apis.Auth.GoogleJsonWebSignature;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace WebAssgn_team06.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private JudgeDAL judgeContext = new JudgeDAL();

        private CompetitorDAL competitorContext = new CompetitorDAL();

        private IHostingEnvironment _env;
        private string _dir;
        public HomeController(ILogger<HomeController> logger, IHostingEnvironment env)
        {
            _logger = logger;
            _env = env;
            _dir = _env.ContentRootPath;
        }

        // GET: Quote
        public async Task<ActionResult> Index()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://api.quotable.io");
            HttpResponseMessage response = await client.GetAsync("/random");
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                var details = JObject.Parse(data);
                var quote = details["content"];
                var author = details["author"];
                ViewData["quote"] = quote;
                ViewData["author"] = author;
                return View();
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult StaffLogin(IFormCollection formData)
        {
            // Read inputs from textboxes
            // Email address converted to lowercase
            string loginID = formData["txtLoginID"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();
            if (loginID == "abc@npbook.com" && password == "pass1234")
            {
                // Store Login ID in session with the key “LoginID”
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role “Staff” as a string in session with the key “Role”
                HttpContext.Session.SetString("Role", "Staff");

                // Redirect user to the "StaffMain" view through an action
                return RedirectToAction("StaffMain");
            }
            else if (GetJudgeLogin(loginID, password))
            {
                // Store Login ID in session with the key “LoginID”
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role “Staff” as a string in session with the key “Role”
                HttpContext.Session.SetString("Role", "Judge");

                // Redirect user to the "StaffMain" view through an action
                return RedirectToAction("JudgeMain");
            }
            else if (GetCompetitorLogin(loginID, password))
            {
                // Store Login ID in session with the key “LoginID”
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role “Staff” as a string in session with the key “Role”
                HttpContext.Session.SetString("Role", "Competitor");

                // Redirect user to the "CompetitorMain" view through an action
                return RedirectToAction("CompetitorMain");
            }
            else if (loginID == "admin1@lcu.edu.sg" && password == "p@55Admin")
            {
                // Store Login ID in session with the key “LoginID”
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role “Staff” as a string in session with the key “Role”
                HttpContext.Session.SetString("Role", "Admin");

                // Redirect user to the "AdminMain" view through an action
                return RedirectToAction("StaffMain");
            }
            else
            {
                // Store an error message in TempData for display at the index view
                TempData["Message"] = "Invalid Login Credentials!";
                // Redirect user back to the index view through an action
                return RedirectToAction("Index");
            }
        }

        private bool GetJudgeLogin(string id, string pass)
        {
            List<Judge> judgeList = judgeContext.GetAllJudge();
            for (int i = 0; i < judgeList.Count; i++)
            {
                if (judgeList[i].Email == id && judgeList[i].Password == pass)
                {
                    return true;
                }
            }
            return false;
        }

        private bool GetCompetitorLogin(string id, string pass)
        {
            List<Competitor> competitorList = competitorContext.GetAllCompetitor();
            for (int i = 0; i < competitorList.Count; i++)
            {
                if (competitorList[i].Email == id && competitorList[i].Password == pass)
                {
                    return true;
                }
            }
            return false;
        }

        public ActionResult StaffMain()
        {
            return View();
        }

        public ActionResult JudgeMain()
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Judge"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult CompetitorMain()
        {
            // Stop accessing the action if not logged in
            // or account not in the "Judge" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult SignUp()
        {
            return View();
        }

        public async Task<ActionResult> LogOut()
        {
            // Clear authentication cookie
            await HttpContext.SignOutAsync(
            CookieAuthenticationDefaults.AuthenticationScheme);

            // Clear all key-values pairs stored in session state
            HttpContext.Session.Clear();
            // Call the Index action of Home controller
            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Authorize]
        public async Task<ActionResult> CompetitorLogin()
        {
            // The user is already authenticated, so this call won't
            // trigger login, but it allows us to access token related values.
            AuthenticateResult auth = await HttpContext.AuthenticateAsync();
            string idToken = auth.Properties.GetTokenValue(OpenIdConnectParameterNames.IdToken);
            try
            {
                // Verify the current user logging in with Google server
                // if the ID is invalid, an exception is thrown
                Payload currentUser = await
                GoogleJsonWebSignature.ValidateAsync(idToken);
                string userName = currentUser.Name;
                string eMail = currentUser.Email;
                HttpContext.Session.SetString("LoginID", userName + " / "
                + eMail);
                HttpContext.Session.SetString("Role", "Competitor");
                HttpContext.Session.SetString("LoggedInTime",
                DateTime.Now.ToString());
                return RedirectToAction("CompetitorMain");
            }
            catch (Exception e)
            {
                // Token ID is may be tempered with, force user to logout
                return RedirectToAction("LogOut");
            }
        }
        public IActionResult MultipleFiles(IEnumerable<IFormFile> files)
        {
            int i = 0;
            foreach (var file in files)
            {
                using (var fileStream = new FileStream(Path.Combine(_dir, $"file{i++}.png"), FileMode.Create, FileAccess.Write))
                {
                    file.CopyTo(fileStream);
                }
            }

            return RedirectToAction("Index");
        }
    }
}