﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.DAL;
using WebAssgn_team06.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Data.SqlClient;

namespace WebAssgn_team06.Controllers
{
    public class AreaInterestController : Controller
    {
        private AreaInterestDAL aContext = new AreaInterestDAL();
        // GET: AreaInterestController
        public ActionResult Index()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<AreaInterest> aList = aContext.GetAllAreaInterest();
            return View(aList);
        }

        // GET: AreaInterestController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AreaInterestController/Create
        public ActionResult Create()
        {
            // Stop accessing the action if not logged in
            // or account not in the "Admin" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        // POST: AreaInterestController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AreaInterest a)
        {
            //Get area interest list for drop-down list
            //in case of the need to return to Create.cshtml view
            if (ModelState.IsValid)
            {
                if (aContext.NameExists(a.Name) == false)
                {

                    //Add AreaInterest record to database
                    a.AreaInterestId = aContext.Add(a);
                    //Redirect user to Index view
                    return RedirectToAction("Index");
                }



                else
                {
                    //validation message
                    String errorMsg = "Name already exists.";
                    ViewBag.validation = errorMsg;
                    return View();
                }
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View(a);
            }

        }


        // GET: AreaInterestController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AreaInterestController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: AreaInterestController/Delete/5
        public ActionResult Delete(int? id)
        {
            // Stop accessing the action if not logged in
            // or account not in the "Admin" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            AreaInterest a = aContext.GetDetails(id.Value);
            if (a == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            return View(a);
        }

        // POST: AreaInterestController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(AreaInterest a)
        {
            if (aContext.HasRecord(a.AreaInterestId) == false)
            {
                if (aContext.HasJudge(a.AreaInterestId) == false)
                {
                    // Delete the area interest record from database
                    aContext.Delete(a.AreaInterestId);
                    return RedirectToAction("Index");
                }
                else
                {
                    //validation message
                    String errorMsg = "Judge with this area of interest found. Unable to delete area of interest.";
                    ViewBag.validation = errorMsg;
                    return View();
                }
            }
            else
            {
                //validation message
                String errorMsg = "Competition record found. Unable to delete this area of interest.";
                ViewBag.validation = errorMsg;
                return View();
            }
        }
    }
}