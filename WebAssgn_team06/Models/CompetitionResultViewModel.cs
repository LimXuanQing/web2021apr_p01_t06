﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssgn_team06.Models
{
    public class CompetitionResultViewModel
    {
        public List<Competition> competitionList { get; set; }
        public List<CompetitionSubmission> competitionSubmissionList { get; set; }
        public List<Competitor> competitorList { get; set; }

        public CompetitionResultViewModel()
        {
            competitionList = new List<Competition>();
            competitionSubmissionList = new List<CompetitionSubmission>();
            competitorList = new List<Competitor>();
        }
    }
}
