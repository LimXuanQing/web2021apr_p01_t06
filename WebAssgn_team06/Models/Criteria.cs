﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssgn_team06.Models
{
    public class Criteria
    {
        [Display(Name = "ID")]
        public int CriteriaId { get; set; }

        //here fk -> pk!!!
        [Display(Name = "Competition ID")]
        public int CompetitionId { get; set; }

        [Required(ErrorMessage = "Please enter a name!")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters")]
        public string CriteriaName { get; set; }

        [Display(Name = "Weightage (%)")]
        [Range(1, 100, ErrorMessage = "Weightage must be between 1% to 100%")]
        public int Weightage { get; set; }

    }
}
