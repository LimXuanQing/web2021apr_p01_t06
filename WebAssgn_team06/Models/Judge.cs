﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssgn_team06.Models
{
    public class Judge
    {
        [Display(Name = "ID")]
        public int JudgeId { get; set; }

        [Required(ErrorMessage = "Please enter a name!")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters")]
        public string JudgeName { get; set; }

        public string Salutation { get; set; }

        //here fk -> pk!!!
        [Display(Name = "Area of Interest")]
        public int AreaInterestId { get; set; }

        [Display(Name = "Email Address")]
        [EmailAddress] //validation annotation for email address format
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        public string Email { get; set; }     
        
        [Display(Name = "Password")]
        [StringLength(255, ErrorMessage = "Password cannot exceed 255 characters")]
        public string Password { get; set; }
    }
}
