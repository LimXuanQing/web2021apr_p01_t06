﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssgn_team06.Models
{
    public class CommentViewModel
    {
        public List<Competition> competitionList { get; set; }
        public List<Comment> commentList { get; set; }

        public CommentViewModel()
        {
            competitionList = new List<Competition>();
            commentList = new List<Comment>();
        }
    }
}
