﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace WebAssgn_team06.Models
{
    //Mitha
    public class Competitor
    {
        //CompetitorID
        [Display(Name = "ID")]
        //Auto generated
        public int CompetitorID { get; set; }

        //CompetitorName
        [Required(ErrorMessage = "Please enter a name!")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters")]
        public string CompetitorName { get; set; }

        //Salutation
        [StringLength(5, ErrorMessage = "Salutation cannot exceed 5 characters")]
        public string Salutation { get; set; }

        //EmailAddr
        [Display(Name = "Email Address")]
        [EmailAddress] //validation annotation for email address format
        // Custom Validation Attribute for checking email address exists
        //[ValidateEmailExistCompetitor] //edit
        [Required(ErrorMessage = "Please enter a Email!")]
        [StringLength(50, ErrorMessage = "Email cannot exceed 50 characters")]
        public string Email { get; set; }

        //Password
        [Required(ErrorMessage = "Please enter a Password!")]
        [StringLength(255, ErrorMessage = "Password cannot exceed 255 characters")]
        public string Password { get; set; }
    }
}
