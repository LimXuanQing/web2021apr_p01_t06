﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssgn_team06.Models
{
    public class Comment
    {
        //pk
        [Display(Name = "Comment ID")]
        public int CommentId { get; set; }

        //pk, fk-> competition (CompetitionID)
        [Display(Name = "Competition ID")]
        public int CompetitionId { get; set; }

        [Display(Name = "Description")]
        [StringLength(255, ErrorMessage = "Name cannot exceed 255 characters")]
        public string Description { get; set; }

        //Singapore Time Zone
        [Display(Name = "Date Time Posted")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm}")]
        public DateTime? DateTimePosted { get; set; } = DateTime.Now;

    }
}
