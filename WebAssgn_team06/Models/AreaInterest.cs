﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssgn_team06.Models
{
    public class AreaInterest
    {
        [Display(Name = "Area of Interest ID")]
        public int AreaInterestId { get; set; }

        [Display(Name = "Area of Interest Name")]
        [Required(ErrorMessage = "Please enter a name!")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters")]
        public string Name { get; set; }

    }
}
