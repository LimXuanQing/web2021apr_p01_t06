﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WebAssgn_team06.DAL;


namespace WebAssgn_team06.Models
{
    public class ValidateEmailExists : ValidationAttribute
    {
        private JudgeDAL judgeContext = new JudgeDAL();

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            // Get the email value to validate
            string email = Convert.ToString(value);
            // Casting the validation context to the "Judge" model class
            Judge judge = (Judge)validationContext.ObjectInstance;
            // Get the Judge Id from the judge instance
            int judgeId = judge.JudgeId;
            if (judgeContext.IsEmailExist(email, judgeId))
                // validation failed
                return new ValidationResult
                ("Email address already exists!");
            else
                // validation passed
                return ValidationResult.Success;
        }
    }
}


