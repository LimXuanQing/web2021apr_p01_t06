﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssgn_team06.Models
{
    public class CompetitionScoreViewModel
    {
        public List<Competition> competitionList { get; set; }
        public List<CompetitionSubmission> competitionSubmissionsList { get; set; }
        public List<CompetitionScore> competitionScoreList { get; set; }

        public CompetitionScoreViewModel()
        {
            competitionList = new List<Competition>();
            competitionSubmissionsList = new List<CompetitionSubmission>();
            competitionScoreList = new List<CompetitionScore>();
        }
    }
}
