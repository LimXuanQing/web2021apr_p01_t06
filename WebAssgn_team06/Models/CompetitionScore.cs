﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebAssgn_team06.Models
{
    public class CompetitionScore
    {
        //pk, fk->  Criteria (CriteriaID)
        [Display(Name = "Criteria ID")]
        public int CriteriaId { get; set; }

        //pk, fk->  Competitor (CompetitorID)
        [Display(Name = "Competitor ID")]
        //Auto generated
        public int CompetitorId { get; set; }

        // here fk->  Competition (CompetitionID) !!!
        [Display(Name = "Competition ID")]
        public int CompetitionId { get; set; }

        //Score
        [Display(Name = "Score")]
        [Range(1, 10, ErrorMessage = "Please enter a value in between 1 and 10")]
        public int Score { get; set; }

        //DateTimeLastEdit
        //Singapore time zone
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{yyyy-mm-dd hh:mm}")]
        public DateTime DateTimeLastEdit { get; set; } = DateTime.Now;

        public string Email { get; set; }

    }
}
