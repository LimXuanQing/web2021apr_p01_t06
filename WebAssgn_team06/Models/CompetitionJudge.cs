﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebAssgn_team06.Models
{
    public class CompetitionJudge
    {
        //pk, fk-> Competition (CompetitionID)
        [Display(Name = "Competition ID")]
        public int CompetitionId { get; set; }
        [Display(Name = "Competition Name")]
        public String CompetitionName { get; set; }

        //pk, fk-> Judge (JudgeID)
        [Display(Name = "Judge ID")]
        public int JudgeId { get; set; }
        [Display(Name = "Judge Name")]
        public String JudgeName { get; set; }

    }
}