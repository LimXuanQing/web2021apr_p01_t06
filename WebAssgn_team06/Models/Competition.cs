﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssgn_team06.Models
{
    public class Competition : IValidatableObject
    {
        [Display(Name = "ID")]
        public int CompetitionId { get; set; }

        
        [Display(Name = "Area of Interest")]
        public int AreaInterestId { get; set; }

        [Required(ErrorMessage = "Please enter a name!")]
        [Display(Name = "Name of competition")]
        [StringLength(255, ErrorMessage = "Name cannot exceed 255 characters")]
        public string CompetitionName { get; set; }

        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Result Release Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime ResultReleasedDate { get; set; }



        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            var end = new[] { "EndDate" };

            if (EndDate < StartDate)
            {
                yield return new ValidationResult("End Date must be greater than Start Date", end);

            }
            var result = new[] { "ResultReleasedDate" };
            if (ResultReleasedDate < EndDate)
            {
                yield return new ValidationResult("Result released date must be greater than End Date", result);
            }

        }



    }
}
