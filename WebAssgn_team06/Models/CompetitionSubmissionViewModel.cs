﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssgn_team06.Models
{
    public class CompetitionSubmissionViewModel
    {
        //pk, fk-> competition (CompetitionID)
        [Display(Name = "Competition ID")]
        public int CompetitionId { get; set; }

        //pk, fk-> competition (CompetitorID)
        [Display(Name = "Competitor ID")]
        public int CompetitorId { get; set; }

        [Display(Name = "FileSubmitted")]
        [StringLength(255, ErrorMessage = "Name cannot exceed 255 characters")]
        public string FileSubmitted { get; set; }
        public IFormFile fileToUpload { get; set; }

        [Display(Name = "Date Time File Upload")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateTimeFileUpload { get; set; }

        [Display(Name = "Appeal")]
        [StringLength(255, ErrorMessage = "Name cannot exceed 255 characters")]
        public string Appeal { get; set; }

        [Display(Name = "Vote Count")]
        public int VoteCount { get; set; }

        [Display(Name = "Ranking")]
        public int? Ranking { get; set; }

    }
}
