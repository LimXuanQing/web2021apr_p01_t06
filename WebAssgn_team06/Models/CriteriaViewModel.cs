﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAssgn_team06.Models
{
    public class CriteriaViewModel
    {
        public List<Competition> competitionList { get; set; }

        public List<Criteria> criteriaList { get; set; }

        public CriteriaViewModel()
        {
            competitionList = new List<Competition>();
            criteriaList = new List<Criteria>();
        }
    }
}
