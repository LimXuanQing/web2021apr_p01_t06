﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.DAL
{
    public class JudgeDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public JudgeDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "JudgeishConnectionString");

            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Judge> GetAllJudge()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Judge ORDER BY JudgeID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a judge list
            List<Judge> judgeList = new List<Judge>();
            while (reader.Read())
            {
                judgeList.Add(
                    new Judge
                    {
                        JudgeId = reader.GetInt32(0), //0: 1st column
                        JudgeName = reader.GetString(1), //1: 2nd column
                        Salutation = reader.GetString(2), //2: 3rd column
                        //3 - 4th column, assign AreaInterestId,
                        AreaInterestId = reader.GetInt32(3),
                        Email = reader.GetString(4), //4: 5th column
                        Password = reader.GetString(5), //5: 6th column  
                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return judgeList;
        }

        //Insert new judge
        public int Add(Judge judge)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated JudgeID after insertion
            cmd.CommandText = @"INSERT INTO Judge (JudgeName, Salutation,
                                 AreaInterestID, EmailAddr, Password) OUTPUT INSERTED.JudgeID
                                VALUES(@name, @salutation, @areaInterestId, @email, @password)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", judge.JudgeName);
            cmd.Parameters.AddWithValue("@salutation", judge.Salutation);
            cmd.Parameters.AddWithValue("@areaInterestId", judge.AreaInterestId);
            cmd.Parameters.AddWithValue("@email", judge.Email);
            cmd.Parameters.AddWithValue("@password", judge.Password);

            //A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //JudgeID after executing the INSERT SQL statement
            //here
            judge.JudgeId = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return judge.JudgeId;
        }

        public bool IsEmailExist(string email, int judgeId)
        {
            bool emailFound = false;
            //Create a SqlCommand object and specify the SQL statement
            //to get a judge record with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT JudgeID FROM Judge
                                WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetInt32(0) != judgeId)
                        //The email address is used by another staff
                        emailFound = true;
                }
            }
            else
            { //No record
                emailFound = false; // The email address given does not exist
            }
            reader.Close();
            conn.Close();

            return emailFound;
        }

        public Judge GetDetails(int judgeId)
        {
            Judge judge = new Judge();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that
            //retrieves all attributes of a judge record.
            cmd.CommandText = @"SELECT * FROM Judge
                                WHERE JudgeID = @selectedJudgeID";

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “judgeId”.
            cmd.Parameters.AddWithValue("@selectedJudgeID", judgeId);

            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill judge object with values from the data reader
                    judge.JudgeId = judgeId;
                    judge.JudgeName = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    judge.Salutation = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    judge.AreaInterestId = (int)(!reader.IsDBNull(3) ?
                                    reader.GetInt32(3) : (int?)null);
                    judge.Email = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                    judge.Password = !reader.IsDBNull(5) ? reader.GetString(5) : null;
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();

            return judge;
        }

        public int Delete(int judgeId)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            //to delete a judge record specified by a Judge ID
            SqlCommand cmd = conn.CreateCommand();
            SqlCommand cmd2 = conn.CreateCommand();
            cmd2.CommandText = @"DELETE FROM Judge
                                WHERE JudgeID = @selectJudgeID";
            cmd.CommandText = @"DELETE FROM CompetitionJudge
                                    WHERE JudgeID = @selectJudgeID";

            cmd.Parameters.AddWithValue("@selectJudgeID", judgeId);
            cmd2.Parameters.AddWithValue("@selectJudgeID", judgeId);

            //Open a database connection
            conn.Open();
            int rowAffected = 0;
            //Execute the DELETE SQL to remove the staff record
            rowAffected += cmd.ExecuteNonQuery();
            rowAffected += cmd2.ExecuteNonQuery();
            //Close database connection
            conn.Close();
            //Return number of row of staff record updated or deleted
            return rowAffected;
        }

        // Return number of row updated
        public int Update(Judge judge)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE Judge SET JudgeName=@name,
                                Salutation=@salutation, AreaInterestID = @aiid,
                                EmailAddr=@email, Password=@pass
                                WHERE JudgeID = @selectedJudgeID";

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", judge.JudgeName);
            cmd.Parameters.AddWithValue("@salutation", judge.Salutation);

            if (judge.AreaInterestId != 0)
                // A branch is assigned
                cmd.Parameters.AddWithValue("@aiid", judge.AreaInterestId);
            else // No branch is assigned
                cmd.Parameters.AddWithValue("@aiid", DBNull.Value);

            cmd.Parameters.AddWithValue("@email", judge.Email);
            cmd.Parameters.AddWithValue("@pass", judge.Password);

            cmd.Parameters.AddWithValue("@selectedJudgeID", judge.JudgeId);

            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection
            conn.Close();

            return count;
        }
    }
}
