﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.DAL
{
    public class CompetitionScoreDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public CompetitionScoreDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "JudgeishConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<CompetitionScore> GetAllCompetitionScore()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM CompetitionScore ORDER BY CompetitorID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<CompetitionScore> competitionScoreList = new List<CompetitionScore>();
            while (reader.Read())
            {
                competitionScoreList.Add(
                    new CompetitionScore
                    {
                        CriteriaId = reader.GetInt32(0), //0: 1st column
                        CompetitorId = reader.GetInt32(1), //1: 2nd column
                        CompetitionId = reader.GetInt32(2), //2: 3rd column
                        Score = reader.GetInt32(3), //3: 4th column

                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return competitionScoreList;
        }

        public List<CompetitionScore> GetAllCompetitionScore2()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM CompetitionScore INNER JOIN Competitor ON CompetitionScore.CompetitorID = Competitor.CompetitorID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<CompetitionScore> competitionScoreList = new List<CompetitionScore>();
            while (reader.Read())
            {
                competitionScoreList.Add(
                    new CompetitionScore
                    {
                        CriteriaId = reader.GetInt32(0), //0: 1st column
                        CompetitorId = reader.GetInt32(1), //1: 2nd column
                        CompetitionId = reader.GetInt32(2), //2: 3rd column
                        Score = reader.GetInt32(3), //3: 4th column
                        Email = reader.GetString(7),

                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return competitionScoreList;
        }

        public CompetitionScore GetDetails(int competitorId, int competitionId)
        {
            CompetitionScore competitionScore = new CompetitionScore();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that
            //retrieves all attributes of a competitor record.
            cmd.CommandText = @"SELECT * FROM CompetitionScore
                                WHERE CompetitorID = @selectedCompetitorID AND CompetitionID = @selectedCompetitionID";

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “CompetitorID”.
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitorId);
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionId);

            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill competition score object with values from the data reader
                    competitionScore.CompetitorId = competitorId;
                    competitionScore.CompetitionId = (int)(!reader.IsDBNull(2) ?
                                    reader.GetInt32(2) : (int?)null);
                    competitionScore.CriteriaId = (int)(!reader.IsDBNull(0) ?
                                    reader.GetInt32(0) : (int?)null);
                    competitionScore.Score = (int)(!reader.IsDBNull(3) ?
                                    reader.GetInt32(3) : (int?)null);
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();

            return competitionScore;
        }

        public int Add(CompetitionScore cs)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO CompetitionScore (CriteriaID, CompetitorID, CompetitionID, Score)
                                OUTPUT INSERTED.CompetitorID
                                VALUES(@criteriaId, @competitorId, @competitionId, @score)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@criteriaId", cs.CriteriaId);
            cmd.Parameters.AddWithValue("@competitorId", cs.CompetitorId);
            cmd.Parameters.AddWithValue("@competitionId", cs.CompetitionId);
            cmd.Parameters.AddWithValue("@score", cs.Score);

            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //CriteriaID after executing the INSERT SQL statement
            cs.CompetitorId = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return cs.CompetitorId;
        }

        // Return number of row updated
        public int Update(CompetitionScore competitionScore)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE CompetitionScore SET Score = @score
                                WHERE CompetitorID = @selectedCompetitorID AND CriteriaID = @selectedCriteriaID";
            //HERE
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@score", competitionScore.Score);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitionScore.CompetitorId);
            cmd.Parameters.AddWithValue("@selectedCriteriaID", competitionScore.CriteriaId);

            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection
            conn.Close();

            return count;
        }


    }
}
