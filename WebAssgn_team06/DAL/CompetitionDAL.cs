﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.DAL
{
    public class CompetitionDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;

        //Constructor
        public CompetitionDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "JudgeishConnectionString");

            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Competition> GetAllCompetition()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Competition ORDER BY CompetitionID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a competition list
            List<Competition> competitionList = new List<Competition>();
            while (reader.Read())
            {
                competitionList.Add(
                    new Competition
                    {
                        CompetitionId = reader.GetInt32(0), //0: 1st column
                        AreaInterestId = reader.GetInt32(1), //1: 2nd column
                        CompetitionName = reader.GetString(2), //2: 3rd column
                        StartDate = reader.GetDateTime(3), //3: 4th column
                        EndDate = reader.GetDateTime(4), //4: 5th column
                        ResultReleasedDate = reader.GetDateTime(5), //5: 6th column

                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return competitionList;
        }

        public int Add(Competition c)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated competitionID after insertion
            cmd.CommandText = @"INSERT INTO Competition (AreaInterestID, CompetitionName, StartDate, EndDate, ResultReleasedDate)
                                OUTPUT INSERTED.CompetitionID
                                VALUES(@areaInterestID, @competitionName, @startDate,@endDate,@resultReleasedDate)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
           
            cmd.Parameters.AddWithValue("@areaInterestID", c.AreaInterestId);
            cmd.Parameters.AddWithValue("@competitionName", c.CompetitionName);
            cmd.Parameters.AddWithValue("@startDate", c.StartDate);
            cmd.Parameters.AddWithValue("@endDate", c.EndDate);
            cmd.Parameters.AddWithValue("@resultReleasedDate", c.ResultReleasedDate);


            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //CompetitionID after executing the INSERT SQL statement
            c.CompetitionId = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return c.CompetitionId;
        }
        public Competition GetDetails(int id)
        {
            Competition c = new Competition();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that 
            //retrieves all attributes of a competition record.
            cmd.CommandText = @"SELECT * FROM Competition
 WHERE CompetitionID = @selectedCompetitionID";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “competitionId”.
            cmd.Parameters.AddWithValue("@selectedCompetitionID", id);
            //Open a database connection

            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    // Fill staff object with values from the data reader 
                    c.CompetitionId = id;
                    c.AreaInterestId = (int)(!reader.IsDBNull(1) ?
                    reader.GetInt32(1) : (int?)null);
                    c.CompetitionName = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    // (char) 0 - ASCII Code 0 - null value
                    c.StartDate = (DateTime)(!reader.IsDBNull(3) ?
                    reader.GetDateTime(3) : (DateTime?)null);
                    c.EndDate = (DateTime)(!reader.IsDBNull(4) ?
                    reader.GetDateTime(4) : (DateTime?)null);
                    c.ResultReleasedDate = (DateTime)(!reader.IsDBNull(5) ?
                    reader.GetDateTime(5) : (DateTime?)null);


                }
            }
            //Close data reader
            reader.Close();
            conn.Close();
            //Close database connection

            return c;
        }
        public int Update(Competition c)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE Competition SET AreaInterestId=@areaInterestId, 
 CompetitionName=@competitionName, StartDate = @startDate, EndDate=@endDate, ResultReleasedDate=@resultReleasedDate 
WHERE CompetitionID = @selectedCompetitionID";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@areaInterestId", c.AreaInterestId);
            cmd.Parameters.AddWithValue("@competitionName", c.CompetitionName);
            cmd.Parameters.AddWithValue("@startDate", c.StartDate);
            cmd.Parameters.AddWithValue("endDate", c.EndDate);
            cmd.Parameters.AddWithValue("@resultReleasedDate", c.ResultReleasedDate);

            cmd.Parameters.AddWithValue("@selectedCompetitionID", c.CompetitionId);
            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection
            conn.Close();
            return count;
        }

        public int Delete(int id)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            //to delete a staff record specified by a competition ID
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM Competition 
 WHERE CompetitionID = @selectCompetitionID";
            cmd.Parameters.AddWithValue("@selectCompetitionID", id);
            //Open a database connection
            conn.Open();
            int rowAffected = 0;
            //Execute the DELETE SQL to remove the competition record
            rowAffected += cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();
            //Return number of row of staff record updated or deleted
            return rowAffected;
        }
        public bool HasCompetitor(int id)
        {


            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that 
            //retrieves all attributes of a competititon record.
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission WHERE CompetitionID = @selectedCompetitionID";


            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “competitionId”.
            cmd.Parameters.AddWithValue("@selectedCompetitionID", id);


            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Close();
                conn.Close();
                //Read the record from database
                return true;
            }
            else
            {
                reader.Close();
                conn.Close();
                return false;
            }
            //Close data reader

            //Close database connection


        }


        public List<Criteria> GetCompetitionCriteria(int competitionId)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all competition
            cmd.CommandText = @"SELECT * FROM Criteria WHERE CompetitionID = @selectedCompetition";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “competitionId”.
            cmd.Parameters.AddWithValue("@selectedCompetition", competitionId);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            List<Criteria> criteriaList = new List<Criteria>();
            while (reader.Read())
            {
                criteriaList.Add(
                    new Criteria
                    {
                        CriteriaId = reader.GetInt32(0), //0: 1st column
                        CompetitionId = reader.GetInt32(1), //1: 2nd column
                        CriteriaName = reader.GetString(2), //2: 3rd column
                        Weightage = reader.GetInt32(3), //3: 4th column
                    }
                );
            }
            //Close DataReader
            reader.Close();
            //Close database connection
            conn.Close();
            return criteriaList;
        }

        public List<CompetitionSubmission> GetCompetitionSubmission(int competitionId)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all competition
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission WHERE CompetitionID = @selectedCompetition";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “competitionId”.
            cmd.Parameters.AddWithValue("@selectedCompetition", competitionId);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            List<CompetitionSubmission> competitionSubmissionList = new List<CompetitionSubmission>();
            while (reader.Read())
            {
                competitionSubmissionList.Add(
                    new CompetitionSubmission
                    {
                        CompetitionId = reader.GetInt32(0), //0: 1st column
                        CompetitorId = reader.GetInt32(1), //1: 2nd column

                        FileSubmitted = !reader.IsDBNull(2) ?
                                    reader.GetString(2) : null, //2: 3rd column --> not sure
                        DateTimeFileUpload = !reader.IsDBNull(3) ?
                                    reader.GetDateTime(3) : (DateTime?)null, //3: 4th column --> not sure
                        Appeal = !reader.IsDBNull(4) ?
                                    reader.GetString(4) : null, //4: 5th column
                        VoteCount = reader.GetInt32(5), //5: 6th column
                        Ranking = !reader.IsDBNull(6) ?
                                    reader.GetInt32(6) : (int?)null, //6: 7th column

                    }
                );
            }
            //Close DataReader
            reader.Close();
            //Close database connection
            conn.Close();
            return competitionSubmissionList;
        }

        public List<Comment> GetCompetitionComment(int competitionId)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all competition
            cmd.CommandText = @"SELECT * FROM Comment WHERE CompetitionID = @selectedCompetition";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “competitionId”.
            cmd.Parameters.AddWithValue("@selectedCompetition", competitionId);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            List<Comment> commentList = new List<Comment>();
            while (reader.Read())
            {
                commentList.Add(
                    new Comment
                    {
                        CommentId = reader.GetInt32(0), //0: 1st column
                        CompetitionId = reader.GetInt32(1), //1: 2nd column
                        Description = !reader.IsDBNull(2) ?
                                    reader.GetString(2) : null, //2: 3rd column 
                        DateTimePosted = !reader.IsDBNull(3) ?
                                    reader.GetDateTime(3) : (DateTime?)null, //3: 4th column 

                    }
                );
            }
            //Close DataReader
            reader.Close();
            //Close database connection
            conn.Close();
            return commentList;
        }

    }
}

