﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.DAL
{
    public class CompetitorDAL
    {

        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public CompetitorDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "JudgeishConnectionString");

            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Competitor> GetAllCompetitor()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Competitor ORDER BY CompetitorID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a Competitor list
            List<Competitor> competitorList = new List<Competitor>();
            while (reader.Read())
            {
                competitorList.Add(
                    new Competitor
                    {
                        CompetitorID = reader.GetInt32(0), //0: 1st column      //Competitor ID
                        CompetitorName = reader.GetString(1), //1: 2nd column   //Competitor Name
                        Salutation = reader.GetString(2), //2: 3rd column       //Salutation
                        Email = reader.GetString(3), //3: 4th column            //Email
                        Password = reader.GetString(4), //3: 4th column  
                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return competitorList;
        }

        //Insert new Competitor
        public int Add(Competitor competitor)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated CompetitorID after insertion
            cmd.CommandText = @"INSERT INTO Competitor (CompetitorName, Salutation,
                                 EmailAddr, Password) OUTPUT INSERTED.CompetitorID
                                VALUES(@name, @salutation, @email, @password)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", competitor.CompetitorName);
            cmd.Parameters.AddWithValue("@salutation", competitor.Salutation);
            cmd.Parameters.AddWithValue("@email", competitor.Email);
            cmd.Parameters.AddWithValue("@password", competitor.Password);

            //A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //CompetitorID after executing the INSERT SQL statement
            //here
            competitor.CompetitorID = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return competitor.CompetitorID;
        }

        public bool IsEmailExist(string email, int competitorId)
        {
            bool emailFound = false;
            //Create a SqlCommand object and specify the SQL statement
            //to get a Competitor record with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT CompetitorID FROM Competitor
                                WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetInt32(0) != competitorId)
                        //The email address is used by another staff
                        emailFound = true;
                }
            }
            else
            { //No record
                emailFound = false; // The email address given does not exist
            }
            reader.Close();
            conn.Close();

            return emailFound;
        }
        public Competitor GetDetails(int competitorId)
        {
            Competitor competitor = new Competitor();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that
            //retrieves all attributes of a competitor record.
            cmd.CommandText = @"SELECT * FROM Competitor
                                WHERE CompetitorID = @selectedCompetitorID";

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “competitorId”.
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitorId);

            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill judge object with values from the data reader
                    competitor.CompetitorID = competitorId;
                    competitor.CompetitorName = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    competitor.Salutation = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    competitor.Email = !reader.IsDBNull(3) ? reader.GetString(3) : null;
                    competitor.Password = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();

            return competitor;
        }
        public int Delete(int competitorId)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            //to delete a competitor record specified by a Competitor ID
            SqlCommand cmd = conn.CreateCommand();
            SqlCommand cmd2 = conn.CreateCommand();
            cmd2.CommandText = @"DELETE FROM Competitor
                                WHERE CompetitorID = @selectCompetitorID";
            cmd.CommandText = @"DELETE FROM CompetitionCompetitor
                                    WHERE CompetitorID = @selectCompetitorID";

            cmd.Parameters.AddWithValue("@selectCompetitorID", competitorId);
            cmd2.Parameters.AddWithValue("@selectCompetitorID", competitorId);

            //Open a database connection
            conn.Open();
            int rowAffected = 0;
            //Execute the DELETE SQL to remove the staff record
            rowAffected += cmd.ExecuteNonQuery();
            rowAffected += cmd2.ExecuteNonQuery();
            //Close database connection
            conn.Close();
            //Return number of row of staff record updated or deleted
            return rowAffected;
        }
        // Return number of row updated
        public int Update(Competitor competitor)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE Competitor SET CompetitorName=@name,
                                Salutation=@salutation,
                                EmailAddr=@email, Password=@pass
                                WHERE CompetitorID = @selectedCompetitorID";

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", competitor.CompetitorName);
            cmd.Parameters.AddWithValue("@salutation", competitor.Salutation);
            cmd.Parameters.AddWithValue("@email", competitor.Email);
            cmd.Parameters.AddWithValue("@pass", competitor.Password);

            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitor.CompetitorID);

            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection
            conn.Close();

            return count;
        }
    }
}
