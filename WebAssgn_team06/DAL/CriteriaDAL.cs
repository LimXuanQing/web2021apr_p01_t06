﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.DAL
{
    public class CriteriaDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;

        //Constructor
        public CriteriaDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "JudgeishConnectionString");

            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Criteria> GetAllCriteria()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Criteria ORDER BY CriteriaID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a criteria list
            List<Criteria> criteriaList = new List<Criteria>();
            while (reader.Read())
            {
                criteriaList.Add(
                    new Criteria
                    {
                        CriteriaId = reader.GetInt32(0), //0: 1st column
                        CompetitionId = reader.GetInt32(1), //1: 2nd column
                        CriteriaName = reader.GetString(2), //2: 3rd column
                        Weightage = reader.GetInt32(3), //3: 4th column
                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return criteriaList;
        }

        public int Add(Criteria criteria)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO Criteria (CompetitionID, CriteriaName, Weightage)
                                OUTPUT INSERTED.CriteriaID
                                VALUES(@competitionId, @name, @weightage)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@competitionId", criteria.CompetitionId);
            cmd.Parameters.AddWithValue("@name", criteria.CriteriaName);
            cmd.Parameters.AddWithValue("@weightage", criteria.Weightage);

            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //CriteriaID after executing the INSERT SQL statement
            criteria.CriteriaId = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return criteria.CriteriaId;
        }

        public Criteria GetDetails(int criteriaId)
        {
            Criteria criteria = new Criteria();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM Criteria
                                WHERE CriteriaID = @selectedCriteriaID";

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “criteriaId”.
            cmd.Parameters.AddWithValue("@selectedCriteriaID", criteriaId);

            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill staff object with values from the data reader
                    criteria.CriteriaId = criteriaId;
                    criteria.CompetitionId = !reader.IsDBNull(1) ?
                                    reader.GetInt32(1) : (int)0;
                    criteria.CriteriaName = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    criteria.Weightage = !reader.IsDBNull(3) ?
                                    reader.GetInt32(3) : (int)0;
                    
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();

            return criteria;
        }

        public int Delete(int criteriaId)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            //to delete a criteria record specified by a Criteria ID
            SqlCommand cmd = conn.CreateCommand();
            SqlCommand cmd2 = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM CompetitionScore
                                    WHERE CriteriaID = @selectCriteriaID";
            cmd2.CommandText = @"DELETE FROM Criteria
                                    WHERE CriteriaID = @selectCriteriaID";

            cmd.Parameters.AddWithValue("@selectCriteriaID", criteriaId);
            cmd2.Parameters.AddWithValue("@selectCriteriaID", criteriaId);
            //Open a database connection
            conn.Open();
            int rowAffected = 0;
            //Execute the DELETE SQL to remove the staff record
            rowAffected += cmd.ExecuteNonQuery();
            rowAffected += cmd2.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return number of row of staff record updated or deleted
            return rowAffected;
        }

    }
}
