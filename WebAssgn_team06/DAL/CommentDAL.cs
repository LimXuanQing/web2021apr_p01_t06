﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.DAL
{
    public class CommentDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;

        //Constructor
        public CommentDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "JudgeishConnectionString");

            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Comment> GetAllComment()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Comment ORDER BY CommentID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a comment list
            List<Comment> commentList = new List<Comment>();
            while (reader.Read())
            {
                commentList.Add(
                    new Comment
                    {
                        CommentId = reader.GetInt32(0), //0: 1st column
                        CompetitionId = reader.GetInt32(1), //1: 2nd column
                        Description = !reader.IsDBNull(2) ?
                                    reader.GetString(2) : null, //2: 3rd column
                        DateTimePosted = !reader.IsDBNull(3) ?
                                    reader.GetDateTime(3) : (DateTime?)null, //3: 4th column

                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return commentList;
        }

        public int Add(Comment ct)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated CommentID after insertion
            cmd.CommandText = @"INSERT INTO Comment(CompetitionID, Description, DateTimePosted)
                                OUTPUT INSERTED.CommentID
                                VALUES(@competitionID, @description, @dateTimePosted)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@competitionID", ct.CompetitionId);
            cmd.Parameters.AddWithValue("@description", ct.Description);
            cmd.Parameters.AddWithValue("@dateTimePosted", ct.DateTimePosted);


            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //CommentID after executing the INSERT SQL statement
            ct.CommentId = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return ct.CommentId;
        }
    }
}
