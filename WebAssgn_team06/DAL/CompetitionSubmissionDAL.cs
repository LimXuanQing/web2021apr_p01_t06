﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.DAL
{
    public class CompetitionSubmissionDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;

        //Constructor
        public CompetitionSubmissionDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "JudgeishConnectionString");

            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<CompetitionSubmission> GetAllCompetitionSubmission()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission ORDER BY CompetitionID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a competitionSubmission list
            List<CompetitionSubmission> competitionSubmissionList = new List<CompetitionSubmission>();
            while (reader.Read())
            {
                competitionSubmissionList.Add(
                    new CompetitionSubmission
                    {
                        CompetitionId = reader.GetInt32(0), //0: 1st column
                        CompetitorId = reader.GetInt32(1), //1: 2nd column

                        FileSubmitted = !reader.IsDBNull(2) ?
                                    reader.GetString(2) : null, //2: 3rd column --> not sure
                        DateTimeFileUpload = !reader.IsDBNull(3) ?
                                    reader.GetDateTime(3) : (DateTime?)null, //3: 4th column --> not sure
                        Appeal = !reader.IsDBNull(4) ?
                                    reader.GetString(4) : null, //4: 5th column
                        VoteCount = reader.GetInt32(5), //5: 6th column
                        Ranking = !reader.IsDBNull(6) ?
                                    reader.GetInt32(6) : (int?)null, //6: 7th column

                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return competitionSubmissionList;
        }

        public int Add(CompetitionSubmission cs)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated CompetitionID after insertion
            cmd.CommandText = @"INSERT INTO CompetitionSubmission (CompetitionID, CompetitorID, FileSubmitted, DateTimeFileUpload, Appeal, VoteCount, Ranking)
                                OUTPUT INSERTED.CompetitionID
                                VALUES(@competitionID, @competitorID, @fileSubmitted, @dateTimeFileUpload, @appeal, @voteCount, @ranking)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.

            //Note: I chose these three variables to test out if it actually makes a new row.
            //File Submission with IFormFile is not done yet.
            //The Competitor Submits the form with these attributes and the judge will access the full tabulated list

            cmd.Parameters.AddWithValue("@competitionID", cs.CompetitionId);
            cmd.Parameters.AddWithValue("@competitorID", cs.CompetitorId);
            cmd.Parameters.AddWithValue("@fileSubmitted", cs.FileSubmitted);
            cmd.Parameters.AddWithValue("@dateTimeFileUpload", cs.DateTimeFileUpload);
            cmd.Parameters.AddWithValue("@appeal", cs.Appeal);
            cmd.Parameters.AddWithValue("@voteCount", cs.VoteCount);
            cmd.Parameters.AddWithValue("@ranking", cs.Ranking);


            //A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //CompetitionID after executing the INSERT SQL statement
            cs.CompetitionId = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations.
            conn.Close();

            //Return id when no error occurs.
            return cs.CompetitionId;
            //
        }

        public CompetitionSubmission GetDetails(int competitorId)
        {
            CompetitionSubmission competitionSubmission = new CompetitionSubmission();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that
            //retrieves all attributes of a competitor record.
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission
                                WHERE CompetitorID = @selectedCompetitorID";

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “CompetitorID”.
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitorId);

            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill competition score object with values from the data reader
                    competitionSubmission.CompetitorId = competitorId;
                    competitionSubmission.CompetitionId = (int)(!reader.IsDBNull(0) ?
                                    reader.GetInt32(0) : (int?)null);
                    competitionSubmission.FileSubmitted = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    competitionSubmission.DateTimeFileUpload = (DateTime)(!reader.IsDBNull(3) ?
                    reader.GetDateTime(3) : (DateTime?)null);
                    competitionSubmission.Appeal = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                    competitionSubmission.VoteCount = (int)(!reader.IsDBNull(5) ?
                                    reader.GetInt32(5) : (int?)null);
                    competitionSubmission.Ranking = (int)(!reader.IsDBNull(6) ?
                                    reader.GetInt32(6) : (int?)null);

                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();

            return competitionSubmission;
        }

        public CompetitionSubmission GetDetails2(int competitorId, int? competitionId)
        {
            CompetitionSubmission competitionSubmission = new CompetitionSubmission();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that
            //retrieves all attributes of a competitor record.
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission
                                WHERE CompetitorID = @selectedCompetitorID AND CompetitionID = @selectedCompetitionID";

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “CompetitorID”.
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitorId);
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionId);

            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill competition score object with values from the data reader
                    competitionSubmission.CompetitorId = competitorId;
                    competitionSubmission.CompetitionId = (int)(!reader.IsDBNull(0) ?
                                    reader.GetInt32(0) : (int?)null);
                    competitionSubmission.FileSubmitted = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    //competitionSubmission.DateTimeFileUpload = (DateTime)(!reader.IsDBNull(3) ?
                    //reader.GetDateTime(3) : (DateTime?)null);
                    competitionSubmission.Appeal = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                    competitionSubmission.VoteCount = (int)(!reader.IsDBNull(5) ?
                                    reader.GetInt32(5) : (int?)null);
                    competitionSubmission.Ranking = (int)(!reader.IsDBNull(6) ?
                                    reader.GetInt32(6) : (int?)null);

                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();

            return competitionSubmission;
        }

        // Used by public to vote for competitors
        public CompetitionSubmission GetDetails3(int competitorId, int? competitionId)
        {
            CompetitionSubmission competitionSubmission = new CompetitionSubmission();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that
            //retrieves all attributes of a competitor record.
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission
                                WHERE CompetitorID = @selectedCompetitorID AND CompetitionID = @selectedCompetitionID";

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “CompetitorID”.
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitorId);
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionId);

            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill competition submission object with values from the data reader
                    competitionSubmission.CompetitorId = competitorId;
                    competitionSubmission.CompetitionId = (int)(!reader.IsDBNull(0) ?
                                    reader.GetInt32(0) : (int?)null);
                    competitionSubmission.FileSubmitted = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    //competitionSubmission.DateTimeFileUpload = (DateTime)(!reader.IsDBNull(3) ?
                    //reader.GetDateTime(3) : (DateTime?)null);
                    competitionSubmission.Appeal = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                    competitionSubmission.VoteCount = (int)(!reader.IsDBNull(5) ?
                                    reader.GetInt32(5) : (int?)null);

                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();

            return competitionSubmission;
        }

        public int Update(CompetitionSubmission competitionSubmission)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE CompetitionSubmission SET Ranking = @ranking
                                WHERE CompetitorID = @selectedCompetitorID AND CompetitionID = @selectedCompetitionID";

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@ranking", competitionSubmission.Ranking);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitionSubmission.CompetitorId);
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionSubmission.CompetitionId);

            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection
            conn.Close();

            return count;
        }

        public List<CompetitionScore> GetCompetitionSubmissionScore(int competitorId)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all competition
            cmd.CommandText = @"SELECT * FROM CompetitionScore WHERE CompetitorID = @selectedCompetitor";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “competitionId”.
            cmd.Parameters.AddWithValue("@selectedCompetitor", competitorId);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            List<CompetitionScore> competitionScoreList = new List<CompetitionScore>();
            while (reader.Read())
            {
                competitionScoreList.Add(
                    new CompetitionScore
                    {
                        CriteriaId = reader.GetInt32(0), //0: 1st column
                        CompetitorId = reader.GetInt32(1), //1: 2nd column
                        CompetitionId = reader.GetInt32(2), //2: 3rd column
                        Score = reader.GetInt32(3), //3: 4th column
                    }
                );
            }
            //Close DataReader
            reader.Close();
            //Close database connection
            conn.Close();
            return competitionScoreList;
        }

        // Used by public to view competitors
        public List<Competitor> GetSubmissionCompetitor(int competitorId)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SQL statement that select all competitors
            cmd.CommandText = @"SELECT * FROM Competitor WHERE CompetitorID = @selectedid";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “competitorId”.
            cmd.Parameters.AddWithValue("@selectedid", competitorId);
            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            List<Competitor> competitorList = new List<Competitor>();
            while (reader.Read())
            {
                competitorList.Add(
                new Competitor
                {
                    CompetitorID = reader.GetInt32(0), //0: 1st column      //Competitor ID
                    CompetitorName = reader.GetString(1), //1: 2nd column   //Competitor Name
                    Salutation = reader.GetString(2), //2: 3rd column       //Salutation
                    Email = reader.GetString(3), //3: 4th column            //Email
                    Password = reader.GetString(4), //3: 4th column  
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close database connection
            conn.Close();
            return competitorList;
        }

        // Used by public to vote for competitors
        public int Vote(CompetitionSubmission competitionSubmission)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify an UPDATE SQL statement
            cmd.CommandText = @"UPDATE CompetitionSubmission SET VoteCount = @vote
                                WHERE CompetitorID = @selectedCompetitorID AND CompetitionID = @selectedCompetitionID";

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@vote", competitionSubmission.VoteCount);
            cmd.Parameters.AddWithValue("@selectedCompetitorID", competitionSubmission.CompetitorId);
            cmd.Parameters.AddWithValue("@selectedCompetitionID", competitionSubmission.CompetitionId);

            //Open a database connection
            conn.Open();
            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();
            //Close the database connection
            conn.Close();

            return count;

        }
    }
}
