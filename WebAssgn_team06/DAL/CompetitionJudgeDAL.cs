﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.DAL
{
    public class CompetitionJudgeDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;

        //Constructor
        public CompetitionJudgeDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "JudgeishConnectionString");

            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }


        public int Add(CompetitionJudge c)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO CompetitionJudge (CompetitionID,JudgeID)

                                
                                VALUES(@competitionId, @judgeId)";

            //            cmd.CommandText = @"DELETE FROM CompetitionJudge
            //WHERE JudgeID = @selectedJudgeID";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@competitionId", c.CompetitionId);
            cmd.Parameters.AddWithValue("@judgeId", c.JudgeId);

            //A connection to database must be opened before any operations made.
            conn.Open();
            int rowAffected = 0;

            rowAffected += cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();
            //Return number of row 
            return rowAffected;
        }

        public CompetitionJudge GetDetails(int id)
        {
            CompetitionJudge cj = new CompetitionJudge();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that 
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM CompetitionJudge
 WHERE JudgeID = @selectedJudgeID";

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “staffId”.
            cmd.Parameters.AddWithValue("@selectedJudgeID", id);



            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill staff object with values from the data reader 
                    cj.CompetitionId = reader.GetInt32(0);
                    cj.JudgeId = id;

                }
            }

            //Close data reader
            reader.Close();
            conn.Close();
            //Close database connection

            return cj;
        }
        public List<CompetitionJudge> GetAllCompetitionJudge()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT Competition.CompetitionID,CompetitionName,Judge.JudgeID,JudgeName FROM CompetitionJudge INNER JOIN Competition ON CompetitionJudge.CompetitionID=Competition.CompetitionID INNER JOIN Judge ON CompetitionJudge.JudgeID=Judge.JudgeID ORDER BY Competition.CompetitionID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a criteria list
            List<CompetitionJudge> cjList = new List<CompetitionJudge>();
            while (reader.Read())
            {
                cjList.Add(
                    new CompetitionJudge
                    {
                        CompetitionId = reader.GetInt32(0), //0: 1st column
                        CompetitionName = reader.GetString(1),
                        JudgeId = reader.GetInt32(2), //2: 3nd column
                        JudgeName = reader.GetString(3)

                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return cjList;
        }
        public int Delete(int id)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            //to delete a staff record specified by a Staff ID
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM CompetitionJudge
WHERE JudgeID = @selectedJudgeID";

            cmd.Parameters.AddWithValue("@selectedJudgeID", id);
            //Open a database connection
            conn.Open();
            int rowAffected = 0;
            //Execute the DELETE SQL to remove the staff record
            rowAffected += cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();
            //Return number of row of staff record updated or deleted
            return rowAffected;
        }
        public bool beforeEndDate(int id)
        {


            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that 
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT EndDate FROM Competition WHERE CompetitionID = @selectedCompetitionID";


            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “sareainterestId”.
            cmd.Parameters.AddWithValue("@selectedCompetitionID", id);


            //Open a database connection
            conn.Open();
            DateTime now = DateTime.Today;
            DateTime endDate;
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            reader.Read();
            //while (reader.Read())
            // {


            endDate = reader.GetDateTime(0);

            if (now < endDate)
            {
                reader.Close();
                conn.Close();

                return true;
            }
            else
            {
                reader.Close();
                conn.Close();
                return false;
            }

            // }
            // reader.Close();
            //conn.Close();


            //Close data reader

            //Close database connection
            //return false;

        }

        public bool judgeAssigned(int id)
        {


            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that 
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM CompetitionJudge WHERE JudgeID = @selectedJudgeID";


            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “sareainterestId”.
            cmd.Parameters.AddWithValue("@selectedJudgeID", id);


            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //while (reader.Read())
            //{
            if (reader.HasRows)
            {
                reader.Close();
                conn.Close();
                //Read the record from database
                return true;
            }
            else
            {
                reader.Close();
                conn.Close();
                return false;
            }
            //Close data reader

            //Close database connection
            //}
            //reader.Close();
            //conn.Close();

            // return false;

        }


    }
}