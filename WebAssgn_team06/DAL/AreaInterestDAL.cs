﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebAssgn_team06.Models;

namespace WebAssgn_team06.DAL
{
    public class AreaInterestDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;

        //Constructor
        public AreaInterestDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "JudgeishConnectionString");

            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<AreaInterest> GetAllAreaInterest()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM AreaInterest ORDER BY AreaInterestID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            //Read all records until the end, save data into a area interest list
            List<AreaInterest> areaInterestList = new List<AreaInterest>();
            while (reader.Read())
            {
                areaInterestList.Add(
                    new AreaInterest
                    {
                        AreaInterestId = reader.GetInt32(0), //0: 1st column
                        Name = reader.GetString(1), //1: 2nd column

                    }
                );
            }

            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return areaInterestList;
        }

        public int Add(AreaInterest a)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated areaInterestID after insertion
            cmd.CommandText = @"INSERT INTO AreaInterest (Name)
                                OUTPUT INSERTED.AreaInterestID
                                VALUES(@name)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.

            cmd.Parameters.AddWithValue("@name", a.Name);


            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //area interestID after executing the INSERT SQL statement
            a.AreaInterestId = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return a.AreaInterestId;
        }

        public AreaInterest GetDetails(int id)
        {
            AreaInterest a = new AreaInterest();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that 
            //retrieves all attributes of a area interest record.
            cmd.CommandText = @"SELECT * FROM AreaInterest
 WHERE AreaInterestID = @selectedAreaInterestID";

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “areainterestId”.
            cmd.Parameters.AddWithValue("@selectedAreaInterestID", id);

            //Open a database connection

            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill staff object with values from the data reader 
                    a.AreaInterestId = id;
                    a.Name = !reader.IsDBNull(1) ? reader.GetString(1) : null;

                }
            }

            //Close data reader
            reader.Close();
            conn.Close();
            //Close database connection

            return a;
        }

        public int Delete(int id)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            //to delete a areainteret record specified by a Staff ID
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM AreaInterest
 WHERE AreaInterestID = @selectAreaInterestID";
            cmd.Parameters.AddWithValue("@selectAreaInterestID", id);
            //Open a database connection
            conn.Open();
            int rowAffected = 0;
            //Execute the DELETE SQL to remove the staff record
            rowAffected += cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();
            //Return number of row of area interest record updated or deleted
            return rowAffected;
        }
        public bool HasRecord(int id)
        {


            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that 
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM Competition WHERE AreaInterestID = @selectedAreaInterestID";


            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “sareainterestId”.
            cmd.Parameters.AddWithValue("@selectedAreaInterestID", id);


            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Close();
                conn.Close();
                //Read the record from database
                return true;
            }
            else
            {
                reader.Close();
                conn.Close();
                return false;
            }

        }

        public bool NameExists(string nameInput)
        {


            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that 
            //retrieves all attributes of a area interest record.
            cmd.CommandText = @"SELECT * FROM AreaInterest WHERE Name = @NameInput";


            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “area interestId”.
            cmd.Parameters.AddWithValue("@NameInput", nameInput);


            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Close();
                conn.Close();
                //Read the record from database
                return true;
            }
            else
            {
                reader.Close();
                conn.Close();
                return false;
            }


        }
        public bool HasJudge(int id)
        {


            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that 
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM Judge WHERE AreaInterestID = @selectedAreaInterestID";


            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “sareainterestId”.
            cmd.Parameters.AddWithValue("@selectedAreaInterestID", id);


            //Open a database connection
            conn.Open();
            //Execute SELCT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                reader.Close();
                conn.Close();
                //Read the record from database
                return true;
            }
            else
            {
                reader.Close();
                conn.Close();
                return false;
            }


        }

    }
}
